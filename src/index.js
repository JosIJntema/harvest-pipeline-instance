//
// You will need a .env file with the following properties
// BASIC_AUTH_LOGIN - This is the username that Harvest Console should use when calling the certbot and config endpoint
// BASIC_AUTH_PASSWORD - This is the password that Harvest Console should use when calling the certbot and config endpoint

const path = require("path");
const moment = require("moment");
require("dotenv").config({path: path.join(__dirname, "..", ".env")});

const harvestPipeline = require("@graindatagroup/harvest-pipeline");
const port = process.env.PORT || 4000;
const server = harvestPipeline.listen(port, () => {


    console.log(JSON.stringify({
        "timeStamp": moment().toISOString(),
        "success": {
            "message": `Harvest - Pipeline on port ${port}`
        }
    }));
});

//
// Add error listeners
//
const logPM2 = ({
                    error,
                    statusCode = 0,
                    level
                }) => {
    console.log(JSON.stringify({
        level,
        statusCode,
        "timeStamp": moment().toISOString(),
        message: error.message,
        logData: error.detail || {},

    }));
};

const sigs = ['SIGINT', 'SIGTERM', 'SIGQUIT'];
sigs.forEach(sig => {
    process.on(sig, () => {

        logPM2({
            level: "info",
            "error": {
                "message": `Signal: ${sig}`
            }
        });

        // Stops the server from accepting new connections and finishes existing connections.
        server.close((e) => {
            if (e) {
                logPM2({
                    level: "info",
                    "error": {
                        "message": `Signal error: ${sig}`
                    }
                });
                process.exit(1)
            } else {
                logPM2({
                    level: "info",
                    "error": {
                        "message": `Signal success: ${sig}`
                    }
                });

                process.exit(0);
            }
        })
    })
});

process.on('unhandledRejection', error => {
    // Will print "unhandledRejection err is not defined"

    logPM2({
        level: "info",
        "error": {
            "message": error.message,
            "detail": error
        }
    });
});
