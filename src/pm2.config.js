module.exports = {
    apps : [{
        name: "harvest-pipeline",
        script: "./harvest-pipeline/src/index.js",
        instances : "max",
        exec_mode : "cluster",
        env : {
            "PORT": 4000
        }
    }]
};
